<?php

namespace EffectDigital\LaravelNumberValidation;

use Illuminate\Support\ServiceProvider;
use Validator;

class ValidatorServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Validator::extend('gte', function ($attribute, $value, $parameters, $validator) {
            if (!isset($parameters[0])) {
                return false;
            }

            $compare = $parameters[0];

            if (is_numeric($compare)) {
                return (float)$value >= (float)$parameters[0];
            }

            if (!isset($validator->getData()[$compare])) {
                return false;
            }

            return (float)$value >= (float)$validator->getData()[$compare];
        });

        Validator::extend('gt', function ($attribute, $value, $parameters, $validator) {
            if (!isset($parameters[0])) {
                return false;
            }

            $compare = $parameters[0];

            if (is_numeric($compare)) {
                return (float)$value > (float)$parameters[0];
            }

            if (!isset($validator->getData()[$compare])) {
                return false;
            }

            return (float)$value > (float)$validator->getData()[$compare];
        });

        Validator::extend('lte', function ($attribute, $value, $parameters, $validator) {
            if (!isset($parameters[0])) {
                return false;
            }

            $compare = $parameters[0];

            if (is_numeric($compare)) {
                return (float)$value <= (float)$parameters[0];
            }

            if (!isset($validator->getData()[$compare])) {
                return false;
            }

            return (float)$value <= (float)$validator->getData()[$compare];
        });

        Validator::extend('lt', function ($attribute, $value, $parameters, $validator) {
            if (!isset($parameters[0])) {
                return false;
            }

            $compare = $parameters[0];

            if (is_numeric($compare)) {
                return (float)$value < (float)$parameters[0];
            }

            if (!isset($validator->getData()[$compare])) {
                return false;
            }

            return (float)$value < (float)$validator->getData()[$compare];
        });
    }
}
