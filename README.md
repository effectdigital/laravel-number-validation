# Laravel Number Validation

To get started, install the package through composer;

`composer require effectdigital/laravel-number-validation`

Now register the service provider in your `config/app.php`

`EffectDigital\LaravelNumberValidation\ValidatorServiceProvider::class,`

Lastly, create the validation messages for the new rules by adding the following lines to `resources/lang/en/validation.php`

```
'gte' => 'The :attribute must be greater-than or equal to :other',
'gt'  => 'The :attribute must be greater-than :other',
'lte' => 'The :attribute must be less-than or equal to :other',
'lt'  => 'The :attribute must be less-than :other',
```

## Example Usage

```
[
    'range_start' => 'required|integer',
    'range_end' => 'required|integer|gte:range_start',
]
 
// OR
 
[
    'days' => 'required|integer|lte:7',
]
```
